Line number. Expected ; actual ; reflection
1. Access $HOME ; expected

1. Access portfolio folder ; expected
2. Access master branch ; expected
3. Adds changes from greeting branch to master branch ; expected
4. Adds changes from vectors branch to master branch ; expected
5. Adds changes from framework branch to master branch ; expected
6. Create baseconversion branch ; expected
7. Access base conversion branch ; expected
8. Create and access week5 folder ; expected
9. Interpret Makefile from framework directory to create dec2bin folder in current folder ; expected
10. Access dec2bin folder ; expected
11. Compile conv.c file from src folder to create conv.o file in lib folder ; expected
12. Archive conv.o file from lib folder to create libconv.a file in lib folder ; expected
13. Compile dec2bin.c from src folder to create dec2bin file in bin folder ; expected
14. Run function from compiled file on dec2bin_tests file ; expected
15. Access $HOME ; expected
16. Access master branch ; expected
17. Add changes from baseconversion branch to master branch ; expected
18. Create docs folder ; expected
19. Create doxygen file ; create Doxyfile file ; got name wrong
20. Add changes from Doxyfile to git repository ; expected

23. Add changes from submission_answers.md file to git repository ; expected
24. Add changes from _FrontPage.md file to git repository ; expected
25. Commit changes with commit message into git repository ; expected
26. Adds changes from git repository to remote git repository ; expected
