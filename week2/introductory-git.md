Line number. Expected ; actual ; reflection
1. access $HOME ; expected
2. create git folder in portfolio folder ; expected
3. access portfolio folder ; expected
4. output list of all files in portfolio folder ; expected
5. output any changes that need to be committed in the git repository ; expected
6. write “hello” to .gitignore file ; expected
7. add all files that have not been added in portfolio folder to git repository ; expected
8. same as line 5 ; expected
9. add an email to the git repository ; expected
10. add a username to the git repository ; expected
11. commit any changes done to the git repository (add) with commit message ; expected
12. same as line 5 ; expected
13. add git repository to remote git repository ; expected
14. add remote git repository address ; expected
15. set up connection to remote git repository address ; expected
16. same as line 5 ; expected
17. write “# CS1PC20 Portfolio” to readme.md file ; expected
18. add readme.md to git repository ; expected
19. commit readme.md with commit message to git repository ; expected
20. add changes to remote git repository ; expected
21. save login settings to remote git repository ; expected
22. create week2 branch ; expected
23. access week2 branch ; expected
24. create week2 folder ; expected
25. write “# Week 2 exercise observations” to report.md file in week2 folder ; expected
26. same as line 5 ; expected
27. add week2 folder to git repository ; expected
28. commit week2 folder with commit message to git repository ; expected

21. same as line 20 ; expected
22. access master branch ; expected
23. same as line 4 ; expected
24. access week2 branch ; expected
25. same as line 4 ; expected
26. same as line 22 ; expected
27. adds changes from week2 branch to master branch ; expected
28. same as line 4 ; expected
29. same as line 13 ; expected
30. remove week2 folder recursively ; expected
31. remove week1 folder recursively ; expected
32. same as line 4 ; expected
33. same as line 5 ; expected
34. saves changes to history ; expected
35. removes saved changes from history and revert them ; expected
36. same as line 4 ; expected
37. access $HOME ; expected
38. create portfolio_backup folder recursively from portfolio ; expected
39. forcefully remove portfolio folder recursively ; expected
40. same as line 4 ; expected
41. clone git repository from remote git repository ; expected
42. same as line 4 ; expected
