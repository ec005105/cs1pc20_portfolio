Line number. Expected ; actual ; reflection
1. Access $HOME ; expected
2. Access portfolio ; expected
3. Create week3 folder ; expected
4. Create greeting folder in week3 folder ; expected
5. Access greeting folder in week3 folder ; expected
6. Create greeting branch ; expected
7. Access greeting branch ; expected
8. When called will output “Hello World!” and return “0” ; N/A
9. Compile greeting.c to create greeting.o file ; expected
10. Check if greet() does return “0” and returns 0 ; N/A
11. call greet(void) ; N/A
12. Write “greeting.o” to .gitignore file ; expected
13. Write “libgreet.a” to .gitignore file ; expected
14. Revert previous write of “greeting.o” and “libgreet.a” from .gitignore file ; archives greeting.o to create greeting.a file ; Thought rv meant revert
15. Compile test_result.c file to test1 file ; expected
16. Output “Hello world!” ; expected
17. Add changes to git repository ; expected
18. commit added files with commit message ; expected
19. adds changes from git repository to remote git repository ; expected
20. Access master branch ; expected
21. Create vectors branch ; expected
22. Access vectors branch ; expected
23. Access week3 folder ; expected
24. Create vectors folder ; expected
25. Access vectors folder ; expected
26. Assign 3 to SIZ variable and create a variable that takes 3 vectors ; N/A
27. Create a function that has 3 vectors and adds them to the add_vectors variable ; N/A
28. Function that adds x and y values of the 3 vectors to z ; N/A
29. Compile vector.c file to create vector.o file ; expected
30. Archive vector.o file to create libvector.a ; expected
31. Compiles test_vector_add.c file to create test_vector_add1 file ; expected
32. Output “0” ; expected but “0” means no output ; return doesn’t mean echo
33. Adds changes to git repository ; expected
34. Commits changes with commit message ; expected
35. Adds changes from git repository to remote git repository ; expected
36. Error message ; expected
37. Additional variable that takes 3 values ; N/A
38. Additional variable that takes 2 vectors, adds the multiplied x and y and returns them in a for loop ; N/A
39. Creates 2 vectors and plugs them to function from line 38 and outputs returns ; N/A

1. Compile vector.c file to create vector.o file (overwrite existing) ; expected
2. Archive vector.o file to create libvector.a file ; expected
3. Compile test_vector_dot_product.c file to create test_vector_dot_product1 file ; expected
4. Output result from dot_product function ; return 0 so no errors ; N/A
5. Add changes to git repository ; expected
6. Commit changes to git repository with commit message ; expected
7. Adds changes from git repository to remote git repository ; expected
