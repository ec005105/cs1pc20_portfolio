Line number. Expected ; actual ; reflection
1. Access $HOME ; expected

1. Access portfolio folder ; expected
2. Access master branch ; expected
3. Create week4/framework folder ; expected
4. Access framework folder ; expected
5. Create framework branch ; expected
6. Access framework branch ; expected
7. Create and access Makefile file ; expected
8. Output content of Makefile file with special characters ; expected
9. Interpret rules in Makefile file and create test_output file ; expected but instead output test_output folder ; Thought it was a file but seems to be a folder with the interpretations
10. Output folder location ; output folder content ; forgot “which” existed
11. Add Makefile folder to git repository ; expected
12. Commit changes with commit message ; expected
13. Add changes from git repository to remote git repository ; expected
14. Access test_output folder and then src folder ; expected
15. Take content from file and compares it ; N/A
16. Compile test_outputs.c file to create test_outputs file ; expected
17. Output error message as file does not exist ; expected
18. Output error message as no file is selected ; expected

20. Output “OK” ; Compared contents of each file with expected but all failed
21. Add changes from test_outputs.c file to git repository ; expected
22. Add changes from op_test file to git repository ; expected
23. Commit changes to git repository with commit message ; expected
24. Add changes from git repository to remote git repository ; expected
