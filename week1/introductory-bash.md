Line number. Expected ; actual ; reflection
1. creates $HOME/portfolio/week1 folder and accesses it ; expected
2. accesses $HOME ; expected
3. removes portfolio folder recursively ; expected
4. same as line 1 (&) ; error message ; the second part of the code executed in the background
5. same as line 2 ; created folder from previous line while accessing $HOME
6. same as line 3 ; expected
7. same as line 1 (&&) ; expected
8. output “Hello World” ; expected
9. error message as no Hello and World variables ; output “Hello, World” ; Seems like echo doesn’t care about syntax if “ signs doe not exist
10. output “Hello, world” and calls Foo bar command ; expected but Foo bar produced error message ; Not an actual command
11. output “Hello, world!” ; expected
12. output “line one” and “line two” ; expected
13. output “Hello, world > readme” ; expected
14. assign “Hello, world” to readme variable ; expected but to readme file ; Thought it looked like pseudocode variable assignment
15. output contents in readme ; expected
16. assign “Hello, World” to example variable ; expected
17. output example variable ; expected
18. same as line 17 ; output “$example” ; ‘’ acted like “”
19. same as line 17 ; expected
20. output “Please enter your name.” and takes input to example variable ; expected
21. output “Hello, <example variable>” ; expected
22. assign 3 to three variable and output three variable ; expected but assigned “1+1+1” ; Seems like C took that as a string instead of arithmetic calculation
23. no clue ; output copyright text ; did not expect it at all
24. output “3” ; expected
25. assign “3” to three variable and output three variable ; expected
26. output current date ; output “date” ; Completely forgot that echo doesn’t care if no special symbols are present
27. output calendar ; expected
28. allow user to choose calendar format ; output folder where cal is in ; Seems to show “which” directory
29. same as line 27 ; expected
30. same as line 29 ; expected
31. same as line 28 (actual) ; output error message ; I suppose no echo so doesn’t matter
32. output “The date is <current date>” ; expected
33. output 0 to 9 ; expected
34. output 0 to 9 and no idea ; output number of numbers in sequence ; wc seems to count number of elements or characters/words
35. write 0 to 9 to sequence file ; expected
36. output number of words and numbers in sequence file ; expected
37. loops through sequence and output 1 to 9 ; expected
38. assign “0” to -n and while looping through sequence, adds the numbers to -n and output sum of sequence ; expected
39. write <string> to hello.c file ; expected
40. output content in hello.c file ; expected
41. compile hello.c file to hello file ; expected
42. calls hello file ; expected
